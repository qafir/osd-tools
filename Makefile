LIBS=-lxosd
PROGRAMS=osd-volume
CC=gcc
CFLAGS=-g0 -O2
LDFLAGS=

all: $(PROGRAMS)

$(PROGRAMS): %: %.c
	$(CC) $(CFLAGS) $(LDFLAGS) $(LIBS) -o $@ $<

clean:
	$(RM) $(PROGRAMS)
